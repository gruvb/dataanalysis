package com.cognitree.gaurav.DataAnalysis;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DataAnalysisTest {
    public static void main(String[] args) throws IOException {
        String fileLocation = System.getProperty("user.dir") +
                "/src/main/java/yoochoose-buys.dat";
        File file = new File(System.getProperty("user.dir") +
                "/src/main/java/yoochoose-buys.dat");
        if(file.exists()) {
            DataAnalysis dataAnalysis = new DataAnalysis(fileLocation);
            dataAnalysis.readFile();
        } else {
            throw new FileNotFoundException();
        }
    }
}
