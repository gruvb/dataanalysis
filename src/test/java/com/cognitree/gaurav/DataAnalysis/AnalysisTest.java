package com.cognitree.gaurav.DataAnalysis;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;


class AnalysisTest {
    protected Map<Integer, Integer>
            itemidVsDistinctSessionCount = new HashMap<>();
    private Set<Integer> sessions = new HashSet<>();
    protected List<List<Integer>>
            itemidVsPurchaseCount = new ArrayList<>();

    @Test
    void getItemIdVsDistinictSessions() {
        int session = 12345;
        int itemId = 42452;
        if (itemidVsDistinctSessionCount.containsKey(itemId)) {
            if(!sessions.contains(session)) {
                itemidVsDistinctSessionCount.
                        put(itemId, itemidVsDistinctSessionCount
                                .get(itemId) + 1);
            }
        } else {
            itemidVsDistinctSessionCount.put(itemId, 1);
        }
        sessions.add(session);
        Assertions.assertNotNull(sessions.size());
    }

    @Test
    void writeReportsToCsv() {
        int itemId = 12345;
        int quantity = 42452;
        List<Integer> itemIdVsQuantity = new ArrayList<>();
        itemIdVsQuantity.add(itemId);
        itemIdVsQuantity.add(quantity);
        itemidVsPurchaseCount.add(itemIdVsQuantity);
        Assertions.assertNotNull(itemidVsPurchaseCount);
    }

}