package com.cognitree.gaurav.streams;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DataAnalysisTest {
    public static void main(String[] args) throws IOException {
        String fileLocation = "/Users/gc/IdeaProjects/" +
                "DataAnalysis/src/main/java/yoochoose-buys.dat";
        File file = new File(fileLocation);
        if(file.exists()) {
            DataAnalysis dataAnalysis = new DataAnalysis(fileLocation);
            dataAnalysis.readFile();
        } else {
            throw new FileNotFoundException();
        }
    }
}
