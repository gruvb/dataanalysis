package com.cognitree.gaurav.streams;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

class AnalysisTests {
    String fileLocation = "/Users/gc/IdeaProjects/" +
            "DataAnalysis/src/main/java/yoochoose-buys.dat";


    @Test
    void ensureSize() throws IOException {
        DataAnalysis dataAnalysis = new DataAnalysis(fileLocation);
        List<List<String>> records = dataAnalysis.readFile();
        Analysis analysis = new Analysis(records);
        analysis.evaluate();
        Assertions.assertEquals(analysis.itemidVsAvgQuantityProvider.itemidVsAvgQuantityPerSession.size() , 19949);
        Assertions.assertEquals(analysis.itemidVsDistinctSessionProvider.itemidVsDistinctSessionCount.size() , 19949);
        Assertions.assertEquals(analysis.itemidVsPurchaseCountProvider.itemidVsPurchaseCount.size() , 19949);
    }

}