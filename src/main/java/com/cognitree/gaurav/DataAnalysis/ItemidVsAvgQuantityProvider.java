package com.cognitree.gaurav.DataAnalysis;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class ItemidVsAvgQuantityProvider {
    private static final Logger logger = LogManager.getLogger(ItemidVsAvgQuantityProvider.class);

    protected Map<Integer, Double>
            itemidVsAvgQuantityPerSession = new HashMap<>();
    private Map<Integer, Integer>
            itemidVsDistinctSessionCount = new HashMap<>();
    private Set<Integer> sessions = new HashSet<>();

    private void getItemIdvsDistinctSessions(PurchaseEvent purchaseEvent){
        int session = purchaseEvent.getSessionId();
        int itemId = purchaseEvent.getItemId();
        if (itemidVsDistinctSessionCount.containsKey(itemId)) {
            if(!sessions.contains(session)) {
                itemidVsDistinctSessionCount.
                        put(itemId, itemidVsDistinctSessionCount
                                .get(itemId) + 1);
            }
        } else {
            itemidVsDistinctSessionCount.put(itemId, 1);
        }
        sessions.add(session);
    }


    protected void calculateReportThree(PurchaseEvent purchaseEvent){
        getItemIdvsDistinctSessions(purchaseEvent);
        int itemId = purchaseEvent.getItemId();
        int quantity = purchaseEvent.getQuantity();
        if (itemidVsAvgQuantityPerSession.containsKey(itemId)) {
            double value = (itemidVsAvgQuantityPerSession.
                    get(itemId) + quantity)/itemidVsDistinctSessionCount.get(itemId);
            itemidVsAvgQuantityPerSession.
                    put(itemId, (int)(value * 100 + 0.5) / 100.0);
        } else {
            itemidVsAvgQuantityPerSession.put(itemId, 0.0);
        }
    }

    protected void writeReportThreeToCsv() throws IOException {
        PrintWriter printWriter = null;
        try {
            String absPath = System.getProperty("user.dir") + "/src/main/java/ReportThree.csv";
            FileWriter fileWriter = new FileWriter(absPath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            for (Map.Entry<Integer, Double> entry : itemidVsAvgQuantityPerSession.entrySet()) {
                String firstElement = String.valueOf(entry.getKey());
                String secondElement = String.valueOf(entry.getValue());
                printWriter.println(firstElement + "," + secondElement);
            }
        } finally {
            printWriter.flush();
            printWriter.close();
        }
    }
}
