package com.cognitree.gaurav.DataAnalysis;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ItemidVsPurchaseCountProvider {
    protected List<List<Integer>>
            itemidVsPurchaseCount = new ArrayList<>();
    protected void calculateReportOne(PurchaseEvent purchaseEvent){
        int itemId = purchaseEvent.getItemId();
        int quantity = purchaseEvent.getQuantity();
        List<Integer> itemIdVsQuantity = new ArrayList<>();
        itemIdVsQuantity.add(itemId);
        itemIdVsQuantity.add(quantity);
        itemidVsPurchaseCount.add(itemIdVsQuantity);
    }
    protected void writeReportOneToCsv() throws IOException {
        PrintWriter printWriter = null;
        try {
            String absPath = System.getProperty("user.dir") + "/src/main/java/ReportOne.csv";
            FileWriter fileWriter = new FileWriter(absPath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            for (int i = 0; i < itemidVsPurchaseCount.size(); i++) {
                String firstElement = String.valueOf(itemidVsPurchaseCount.get(i).get(0));
                String secondElement = String.valueOf(itemidVsPurchaseCount.get(i).get(1));
                printWriter.println(firstElement + "," + secondElement);
            }
        } finally {
            printWriter.flush();
            printWriter.close();
        }
    }
}
