package com.cognitree.gaurav.DataAnalysis;

import java.time.LocalDate;
import java.util.Objects;

public class PurchaseEvent {
    private int sessionId;
    private LocalDate timestamp;
    private int itemId;
    private int price;
    private int quantity;

    @Override
    public String toString() {
        return "PurchaseEvent{" +
                "sessionId=" + sessionId +
                ", timestamp=" + timestamp +
                ", itemId=" + itemId +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseEvent that = (PurchaseEvent) o;
        return sessionId == that.sessionId && itemId == that.itemId && price == that.price && quantity == that.quantity && timestamp.equals(that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionId, timestamp, itemId, price, quantity);
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public LocalDate getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDate timestamp) {
        this.timestamp = timestamp;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
