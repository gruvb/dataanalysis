package com.cognitree.gaurav.DataAnalysis;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class ItemidVsDistinctSessionProvider {
    protected Map<Integer, Integer>
            itemidVsDistinctSessionCount = new HashMap<>();
    private Set<Integer> sessions = new HashSet<>();
    protected void calculateReportTwo(PurchaseEvent purchaseEvent){
        int session = purchaseEvent.getSessionId();
        int itemId = purchaseEvent.getItemId();
        if (itemidVsDistinctSessionCount.containsKey(itemId)) {
            if(!sessions.contains(session)) {
                itemidVsDistinctSessionCount.
                        put(itemId, itemidVsDistinctSessionCount
                                .get(itemId) + 1);
            }
        } else {
            itemidVsDistinctSessionCount.put(itemId, 1);
        }
        sessions.add(session);
    }

    protected void writeReportTwoToCsv() throws IOException {
        PrintWriter printWriter = null;
        try {
            String absPath = System.getProperty("user.dir") + "/src/main/java/ReportTwo.csv";
            FileWriter fileWriter = new FileWriter(absPath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            for (Map.Entry<Integer, Integer> entry : itemidVsDistinctSessionCount.entrySet()) {
                String firstElement = String.valueOf(entry.getKey());
                String secondElement = String.valueOf(entry.getValue());
                printWriter.println(firstElement + "," + secondElement);
            }
        } finally {
            printWriter.flush();
            printWriter.close();
        }

    }
}
