package com.cognitree.gaurav.DataAnalysis;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//logger reports. log4j2.
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DataAnalysis {

private static final Logger logger = LogManager.getLogger(DataAnalysis.class);

    private final String fileLocation;

    public DataAnalysis(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    protected void readFile() throws FileNotFoundException, IOException {
        try (BufferedReader br = new BufferedReader(
                new FileReader(fileLocation))) {
            String line;
            Analysis analysis = new Analysis();
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX");
            while ((line = br.readLine()) != null) {
                PurchaseEvent purchaseEvent = new PurchaseEvent();
                String[] values = line.split(",");
                purchaseEvent.setSessionId(Integer.parseInt(values[0]));
                LocalDate dateTime = LocalDate.parse(values[1], dateTimeFormatter);
                purchaseEvent.setTimestamp(dateTime);
                purchaseEvent.setItemId(Integer.parseInt(values[2]));
                purchaseEvent.setPrice(Integer.parseInt(values[3]));
                purchaseEvent.setQuantity(Integer.parseInt(values[4]));
                analysis.evaluate(purchaseEvent);
            }
            logger.debug("File read");
            analysis.printReports();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            logger.debug("FileNotFoundException occured");
            throw new FileNotFoundException("File not Found");
        } catch (IOException e) {
            logger.debug("IOException occured");
            throw new IOException("IO exception");
        }
    }
}
