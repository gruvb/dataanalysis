package com.cognitree.gaurav.DataAnalysis;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;


public class Analysis {

    private static final int SIZE_OF_RECORD = 5;
    private static final Logger logger = LogManager.getLogger(Analysis.class);

    private ItemidVsPurchaseCountProvider itemidVsPurchaseCountProvider = new ItemidVsPurchaseCountProvider();
    private ItemidVsDistinctSessionProvider itemidVsDistinctSessionProvider = new ItemidVsDistinctSessionProvider();
    private ItemidVsAvgQuantityProvider itemidVsAvgQuantityProvider = new ItemidVsAvgQuantityProvider();

    protected void evaluate(PurchaseEvent purchaseEvent) {

        itemidVsPurchaseCountProvider.calculateReportOne(purchaseEvent);
        itemidVsDistinctSessionProvider.calculateReportTwo(purchaseEvent);
        itemidVsAvgQuantityProvider.calculateReportThree(purchaseEvent);

    }

    protected void writeReportsToCsv() throws IOException {
        logger.debug("Reports written");

        itemidVsPurchaseCountProvider.writeReportOneToCsv();
        itemidVsDistinctSessionProvider.writeReportTwoToCsv();
        itemidVsAvgQuantityProvider.writeReportThreeToCsv();
    }

    protected void printReports() throws IOException {

        logger.debug("Reports Printed");

        System.out.println(itemidVsPurchaseCountProvider.itemidVsPurchaseCount);
        System.out.println(itemidVsDistinctSessionProvider.itemidVsDistinctSessionCount);
        System.out.println(itemidVsAvgQuantityProvider.itemidVsAvgQuantityPerSession);
    }

}
