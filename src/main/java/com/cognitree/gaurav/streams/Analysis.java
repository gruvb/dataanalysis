package com.cognitree.gaurav.streams;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;

public class Analysis {

    private List<List<String>> records;

    private static final Logger logger = LogManager.getLogger(Analysis.class);

    protected ItemidVsPurchaseCountProvider itemidVsPurchaseCountProvider;
    protected ItemidVsDistinctSessionProvider itemidVsDistinctSessionProvider;
    protected ItemidVsAvgQuantityProvider itemidVsAvgQuantityProvider;

    public Analysis(List<List<String>> records) {

        this.records = records;
        itemidVsPurchaseCountProvider = new ItemidVsPurchaseCountProvider(records);
        itemidVsDistinctSessionProvider = new ItemidVsDistinctSessionProvider(records);
        itemidVsAvgQuantityProvider = new ItemidVsAvgQuantityProvider(records);
    }

    protected void evaluate() {

        itemidVsPurchaseCountProvider.calculateReportOne();
        itemidVsDistinctSessionProvider.calculateReportTwo();
        itemidVsAvgQuantityProvider.calculateReportThree();

    }

    protected void writeReportsToCsv() throws IOException {
        logger.debug("Reports written");
        itemidVsPurchaseCountProvider.writeReportOneToCsv();
        itemidVsDistinctSessionProvider.writeReportTwoToCsv();
        itemidVsAvgQuantityProvider.writeReportThreeToCsv();
    }

    protected void printReports() throws IOException {
        logger.debug("Reports Printed");
        System.out.println(itemidVsPurchaseCountProvider.itemidVsPurchaseCount.size());
        System.out.println(itemidVsDistinctSessionProvider.itemidVsDistinctSessionCount.size());
        System.out.println(itemidVsAvgQuantityProvider.itemidVsAvgQuantityPerSession);
    }

}
