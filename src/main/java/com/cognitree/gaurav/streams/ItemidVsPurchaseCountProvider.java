package com.cognitree.gaurav.streams;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ItemidVsPurchaseCountProvider {

    protected Map<Integer, Integer> itemidVsPurchaseCount;
    private List<List<String>> records;

    public ItemidVsPurchaseCountProvider(List<List<String>> records) {
        this.records = records;
    }

    protected void calculateReportOne() {
        itemidVsPurchaseCount = records.stream().
                collect(Collectors.toMap(record->Integer.parseInt(record.get(2)),
                        record-> 1+ Integer.parseInt(record.get(4)),
                        (existing, replacement) -> replacement+1 ));
    }


    protected void writeReportOneToCsv() throws IOException {
        PrintWriter printWriter = null;
        try {
            String absPath = System.getProperty("user.dir") + "/src/main/java/ReportOneStreams.csv";
            FileWriter fileWriter = new FileWriter(absPath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            for (Map.Entry<Integer, Integer> entry : itemidVsPurchaseCount.entrySet()) {
                String firstElement = String.valueOf(entry.getKey());
                String secondElement = String.valueOf(entry.getValue());
                printWriter.println(firstElement + "," + secondElement);
            }
        } finally {
            printWriter.flush();
            printWriter.close();
        }
    }

}
