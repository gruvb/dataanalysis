package com.cognitree.gaurav.streams;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

public class ItemidVsAvgQuantityProvider {

    protected Map<Integer, Double> itemidVsAvgQuantityPerSession;
    private List<List<String>> records;
    private Map<Integer, Integer> itemidVsPurchaseCount;
    private Map<Integer, Integer> itemidVsDistinctSessionCount = new HashMap<>();
    private static Map<Integer, Set<String>> mapItemIdWithSessions = new HashMap<>();

    public ItemidVsAvgQuantityProvider(List<List<String>> records) {
        this.records = records;
    }

    private static void addDistinctSessionsForItems(List<String> record) {
        int itemId = Integer.parseInt(record.get(2));
        String sessionId = record.get(0);
        if(mapItemIdWithSessions.containsKey(itemId)) {
            Set<String > sessionIds = mapItemIdWithSessions.get(itemId);
            if(!sessionIds.contains(sessionId)){
                sessionIds.add(sessionId);
                mapItemIdWithSessions.put(itemId, sessionIds);
            }
        } else {
            Set<String> newSessions  = new HashSet<>();
            newSessions.add(sessionId);
            mapItemIdWithSessions.put(itemId, newSessions);
        }
    }

    protected void calculateReportThree() {
        itemidVsPurchaseCount = records.stream().
                collect(Collectors.toMap(record->Integer.parseInt(record.get(2)),
                        record-> 1+ Integer.parseInt(record.get(4)),
                        (existing, replacement) -> replacement+1 ));

        records.stream().forEach(ItemidVsAvgQuantityProvider::addDistinctSessionsForItems);

        itemidVsDistinctSessionCount = mapItemIdWithSessions
                .entrySet().stream()
                .collect(Collectors.toMap(
                        elem -> elem.getKey(),
                        elem-> elem.getValue().size()
                ));

        itemidVsAvgQuantityPerSession = itemidVsPurchaseCount
                .entrySet().stream()
                .collect(Collectors.toMap(
                        elem -> elem.getKey(),
                        elem-> (int)((elem.getValue() / new Double
                                (itemidVsDistinctSessionCount.get(elem.getKey()))) * 100 + 0.5) / 100.0
                ));
    }

    protected void writeReportThreeToCsv() throws IOException {
        PrintWriter printWriter = null;
        try {
            String absPath = System.getProperty("user.dir") +
                    "/src/main/java/ReportThreeStreams.csv";
            FileWriter fileWriter = new FileWriter(absPath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            for (Map.Entry<Integer, Double> entry : itemidVsAvgQuantityPerSession.entrySet()) {
                String firstElement = String.valueOf(entry.getKey());
                String secondElement = String.valueOf(entry.getValue());
                printWriter.println(firstElement + "," + secondElement);
            }
        } finally {
            printWriter.flush();
            printWriter.close();
        }
    }
}
