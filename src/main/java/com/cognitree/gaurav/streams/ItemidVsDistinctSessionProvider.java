package com.cognitree.gaurav.streams;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.stream.Collectors;

public class ItemidVsDistinctSessionProvider {

    protected Map<Integer, Integer> itemidVsDistinctSessionCount = new HashMap<>();
    private static Map<Integer, Set<String>> mapItemIdWithSessions = new HashMap<>();
    private List<List<String>> records;

    public ItemidVsDistinctSessionProvider(List<List<String>> records) {
        this.records = records;
    }

    private static void addDistinctSessionsForItems(List<String> record) {
        int itemId = Integer.parseInt(record.get(2));
        String sessionId = record.get(0);
        if(mapItemIdWithSessions.containsKey(itemId)) {
            Set<String > sessionIds = mapItemIdWithSessions.get(itemId);
            if(!sessionIds.contains(sessionId)){
                sessionIds.add(sessionId);
                mapItemIdWithSessions.put(itemId, sessionIds);
            }
        } else {
            Set<String> newSessions  = new HashSet<>();
            newSessions.add(sessionId);
            mapItemIdWithSessions.put(itemId, newSessions);
        }
    }

    protected void calculateReportTwo() {

        records.stream().forEach(ItemidVsDistinctSessionProvider::addDistinctSessionsForItems);
        itemidVsDistinctSessionCount = mapItemIdWithSessions
                .entrySet().stream()
                .collect(Collectors.toMap(
                        elem -> elem.getKey(),
                        elem-> elem.getValue().size()
                ));
    }

    protected void writeReportTwoToCsv() throws IOException {
        PrintWriter printWriter = null;
        try {
            String absPath = System.getProperty("user.dir") +
                    "src/main/java/ReportTwoStreams.csv";
            FileWriter fileWriter = new FileWriter(absPath, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            printWriter = new PrintWriter(bufferedWriter);
            for (Map.Entry<Integer, Integer> entry : itemidVsDistinctSessionCount.entrySet()) {
                String firstElement = String.valueOf(entry.getKey());
                String secondElement = String.valueOf(entry.getValue());
                printWriter.println(firstElement + "," + secondElement);
            }
        } finally {
            printWriter.flush();
            printWriter.close();
        }
    }
}
