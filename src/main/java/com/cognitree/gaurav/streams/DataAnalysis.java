package com.cognitree.gaurav.streams;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.NotSerializableException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataAnalysis {
    private final String fileLocation;
    private static final Logger logger = LogManager.getLogger(DataAnalysis.class);

    public DataAnalysis(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    private static final int SIZE_OF_RECORD = 5;

    protected List<List<String>> readFile() throws IOException {

        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(
                new FileReader(fileLocation))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                if(values.length != SIZE_OF_RECORD) {
                    System.out.println("Record Corrupt.");
                    throw new NotSerializableException();
                }
                logger.debug("File read");
                records.add(Arrays.asList(values));
            }
        } catch (IOException e) {
            logger.debug("IOException occured");
            e.printStackTrace();
        }
        Analysis analysis = new Analysis(records);
        analysis.evaluate();
        analysis.printReports();
        analysis.writeReportsToCsv();
        return records;
    }
}
